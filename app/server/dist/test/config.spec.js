"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai = __importStar(require("chai"));
const config_1 = __importDefault(require("../src/config.js"));
const assert = chai.assert;
describe('Config', () => {
    it('exports \'Config\' interface', () => {
        const mockConfig = {
            environment: 'foo',
            port: 1234,
            postgresUser: 'foo',
            postgresPassword: 'bar',
        };
        assert.exists(mockConfig.environment, 'Config Interface expects \'environment\' member');
        assert.isString(mockConfig.environment, 'Expected \'environment\' member to be string');
        assert.exists(mockConfig.port, 'Config Interface expects \'port\' member');
        assert.isNumber(mockConfig.port, 'Expected \'port\' member to be number');
        assert.exists(mockConfig.postgresUser, 'Config Interface expects \'postgresUser\' member');
        assert.isString(mockConfig.postgresUser, 'Expected \'postgresUser\' member to be string');
        assert.exists(mockConfig.postgresPassword, 'Config Interface expects \'postgresPassword\' member');
        assert.isString(mockConfig.postgresPassword, 'Expected \'postgresPassword\' member to be string');
    });
    it('exports configuration object', () => {
        assert.exists(config_1.default.environment, 'Expected \'environment\' to be present in config');
        assert.exists(config_1.default.port, 'Expected \'port\' to be present in config');
        assert.exists(config_1.default.postgresUser, 'Expected \'postgresUser\' to be present in config');
        assert.exists(config_1.default.postgresPassword, 'Expected \'postgresPassword\' to be present in config');
    });
});
//# sourceMappingURL=config.spec.js.map