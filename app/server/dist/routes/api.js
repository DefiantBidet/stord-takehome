"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _router = _interopRequireDefault(require("@koa/router"));

var _api = require("../controllers/api");

var apiRouter = new _router["default"]({
  prefix: '/api'
});
/**
 * GET Attempts to retrieve information of stored links via slug.
 * @param {string}   '/:slug'  Path to handle with slug as route parameter
 * @param {function} checkSlug Controller method callback
 * @function
 */

apiRouter.get('/:slug', _api.checkSlug);
/**
 * POST Attempts to generate a slug from supplied link.
 * @param {string}   '/generate' Path to handle
 * @param {function} saveURL     Controller method callback
 */

apiRouter.post('/generate', _api.saveURL);
var _default = apiRouter;
exports["default"] = _default;