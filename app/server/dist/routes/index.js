"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _router = _interopRequireDefault(require("@koa/router"));

var _controllers = require("../controllers/index.js");

var mainRouter = new _router["default"]({
  prefix: ''
});
mainRouter.get('/:slug', _controllers.redirectToURL);
var _default = mainRouter;
exports["default"] = _default;