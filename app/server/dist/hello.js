'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports['default'] = void 0;

function greet(name) {
  return 'Hello '.concat(name);
}

var _default = greet;
exports['default'] = _default;