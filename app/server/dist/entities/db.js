"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatabaseConnection = void 0;
exports["default"] = create;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _knex = _interopRequireDefault(require("knex"));

var _config = _interopRequireDefault(require("../config"));

// import appConfig from '../config';

/**
 * Knex Database Connection
 * @type {Knex}
 */
var DatabaseConnection = null;
exports.DatabaseConnection = DatabaseConnection;
var knexConfig = {
  client: 'pg',
  connection: {
    host: _config["default"].postgresHost,
    port: _config["default"].postgresPort,
    user: _config["default"].postgresUser,
    password: _config["default"].postgresPassword,
    database: _config["default"].postgresDb
  }
};
/**
 * Asynchronously creates a connection to the Database via Knex
 * @return {Promise<Knex>} Knex connexection object
 * @async
 * @function
 */

function create() {
  return _create.apply(this, arguments);
}

function _create() {
  _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    var connection;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            connection = (0, _knex["default"])(knexConfig);
            exports.DatabaseConnection = DatabaseConnection = connection; // attempt a simple query to verify connection

            _context.prev = 2;
            _context.next = 5;
            return connection.raw('SELECT 1');

          case 5:
            return _context.abrupt("return", connection);

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](2);
            throw new Error('Error connecting Knex to Database. Ensure Database Connection details.');

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[2, 8]]);
  }));
  return _create.apply(this, arguments);
}