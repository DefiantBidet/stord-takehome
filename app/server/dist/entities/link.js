"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLinkBySlug = exports.getLinkByMD5 = exports.createLink = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _db = require("./db");

/**
 * Asynchronous request to write to the database.
 * Attempts to store a new Link entry.
 * @param  {Link}                 link Link data to store
 * @return {Promise<QueryResult>}      Results of query
 * @async
 * @function
 */
var createLink = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(link) {
    var result;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _db.DatabaseConnection)('link').insert(link);

          case 2:
            result = _context.sent;
            return _context.abrupt("return", result);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function createLink(_x) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Asynchronous request to read from the database.
 * Attempts to read an existing Link entry.
 * @param  {string}        value MD5 hash of URL to search
 * @return {Promise<Link>}       Results of query
 * @async
 * @function
 */


exports.createLink = createLink;

var getLinkByMD5 = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(value) {
    var link;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return (0, _db.DatabaseConnection)('link').where('md5', value).first();

          case 2:
            link = _context2.sent;
            return _context2.abrupt("return", link);

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function getLinkByMD5(_x2) {
    return _ref2.apply(this, arguments);
  };
}();
/**
 * Asynchronous request to read from the database.
 * Attempts to read an existing Link entry.
 * @param  {string}        value Slug of Link to search
 * @return {Promise<Link>}       Results of query
 * @async
 * @function
 */


exports.getLinkByMD5 = getLinkByMD5;

var getLinkBySlug = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(value) {
    var link;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _db.DatabaseConnection)('link').where('slug', value).first();

          case 2:
            link = _context3.sent;
            return _context3.abrupt("return", link);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function getLinkBySlug(_x3) {
    return _ref3.apply(this, arguments);
  };
}();

exports.getLinkBySlug = getLinkBySlug;