"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SLUG_LENGTH = exports.BASE62_ALPHABET = void 0;

/**
 * Base62 charset for slug generation
 * @type {String}
 */
var BASE62_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
/**
 * Max length of slug
 * @type {Number}
 */

exports.BASE62_ALPHABET = BASE62_ALPHABET;
var SLUG_LENGTH = 7;
exports.SLUG_LENGTH = SLUG_LENGTH;