"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.redirectToURL = void 0;
const constants_1 = require("../utils/constants.js");
async function redirectToURL(ctx) {
    const { slug } = ctx.params;
    // if the slug is less than 7 char we know we don't need to hit the db.
    // redirect back to the SPA
    if (slug.length < constants_1.SLUG_LENGTH) {
        ctx.redirect('/');
    }
    // TODO: implement
    ctx.status = 200;
    ctx.body = {};
    // TODO:
    //  - take slug and lookup if present
    //    - redirect if found, back to form if not
}
exports.redirectToURL = redirectToURL;
//# sourceMappingURL=index.js.map