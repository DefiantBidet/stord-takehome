"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveURL = void 0;
const crypto_1 = __importDefault(require("crypto"));
const link_1 = require("../entities/link.js");
async function saveURL(ctx) {
    const { url } = ctx.request.body;
    const hash = crypto_1.default.createHash('md5')
        .update(url)
        .digest('hex');
    // check if md5 hash of link is stored in DB
    const md5Exists = await (0, link_1.getLinkByMD5)(hash);
    if (md5Exists === undefined) {
        // TODO: storing has as slug for now - need to encode it
        // not already stored, save it.
        const link = {
            md5: hash,
            slug: hash,
            url: url,
        };
        // const results = await createLink(link);
        // console.log('???>', results);
        // if (results)
    }
    else {
        // return existing link
        ctx.status = 200;
        ctx.body = md5Exists;
        return;
    }
    // TODO: implement
    ctx.status = 200;
    ctx.body = {
        'slug': hash,
        url,
    };
    // TODO:
    // lookup to see if present
    // store if we can
}
exports.saveURL = saveURL;
//# sourceMappingURL=api.js.map