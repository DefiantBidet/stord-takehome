"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLinkBySlug = exports.getLinkByMD5 = void 0;
const db_1 = require("./db.js");
// export const createLink = async (link: Link): Promise<any> => {
//   const result = await DatabaseConnection<Link>('link').insert(link);
//   return result;
// };
const getLinkByMD5 = async (value) => {
    const link = await (0, db_1.DatabaseConnection)('link').where('md5', value).first();
    return link;
};
exports.getLinkByMD5 = getLinkByMD5;
const getLinkBySlug = async (value) => {
    const link = await (0, db_1.DatabaseConnection)('link').where('slug', value).first();
    return link;
};
exports.getLinkBySlug = getLinkBySlug;
//# sourceMappingURL=link.js.map