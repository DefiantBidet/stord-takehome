"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseConnection = void 0;
const knex_1 = __importDefault(require("knex"));
const config_1 = __importDefault(require("../config.js"));
exports.DatabaseConnection = null;
const knexConfig = {
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        port: config_1.default.postgresPort,
        user: config_1.default.postgresUser,
        password: config_1.default.postgresPassword,
        database: config_1.default.postgresDb,
    }
};
async function create() {
    const connection = (0, knex_1.default)(knexConfig);
    exports.DatabaseConnection = connection;
    // attempt a simple query to verify connection
    try {
        // await connection.raw('SELECT 1')
        return connection;
    }
    catch (error) {
        throw new Error('Error connecting Knex to Database. Ensure Database Connection details.');
    }
}
exports.default = create;
//# sourceMappingURL=db.js.map