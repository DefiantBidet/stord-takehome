"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BASE62_ALPHABET = exports.SLUG_LENGTH = void 0;
exports.SLUG_LENGTH = 7;
exports.BASE62_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//# sourceMappingURL=constants.js.map