"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const cors_1 = __importDefault(require("@koa/cors"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const koa_helmet_1 = __importDefault(require("koa-helmet"));
const koa_static_1 = __importDefault(require("koa-static"));
const config_1 = __importDefault(require("./config.js"));
const db_1 = __importDefault(require("./entities/db.js"));
const index_1 = __importDefault(require("./routes/index.js"));
const api_1 = __importDefault(require("./routes/api.js"));
async function createServer() {
    const app = new koa_1.default();
    const db = await (0, db_1.default)();
    app.use((0, cors_1.default)());
    app.use((0, koa_helmet_1.default)());
    app.use((0, koa_bodyparser_1.default)());
    app.use(api_1.default.routes());
    app.use(api_1.default.allowedMethods());
    app.use(index_1.default.routes());
    app.use(index_1.default.allowedMethods());
    app.use((0, koa_static_1.default)('../client/dist'));
    const server = app.listen(config_1.default.serverPort, () => {
        console.log(`Server running on port ${config_1.default.serverPort}`);
    });
    const serverInstance = {
        app,
        db,
        server,
    };
    return serverInstance;
}
exports.default = createServer();
//# sourceMappingURL=server.js.map