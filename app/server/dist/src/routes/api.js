"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = __importDefault(require("@koa/router"));
const api_1 = require("../controllers/api.js");
const apiRouter = new router_1.default({ prefix: '/api' });
apiRouter.post('/generate', api_1.saveURL);
exports.default = apiRouter;
//# sourceMappingURL=api.js.map