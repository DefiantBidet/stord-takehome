"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = __importDefault(require("@koa/router"));
const index_1 = require("../controllers/index.js");
const mainRouter = new router_1.default();
mainRouter.get('/:slug', index_1.redirectToURL);
exports.default = mainRouter;
//# sourceMappingURL=index.js.map