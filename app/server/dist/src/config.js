"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: '../../.env' });
const config = {
    environment: process.env.NODE_ENV,
    postgresDb: process.env.POSTGRES_DB,
    postgresUser: process.env.POSTGRES_USER,
    postgresPassword: process.env.POSTGRES_PASSWORD,
    postgresPort: Number.parseInt(process.env.POSTGRES_PORT, 10),
    serverPort: Number.parseInt(process.env.SERVER_PORT, 10),
};
exports.default = config;
//# sourceMappingURL=config.js.map