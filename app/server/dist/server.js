"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _koa = _interopRequireDefault(require("koa"));

var _cors = _interopRequireDefault(require("@koa/cors"));

var _koaBodyparser = _interopRequireDefault(require("koa-bodyparser"));

var _koaHelmet = _interopRequireDefault(require("koa-helmet"));

var _koaStatic = _interopRequireDefault(require("koa-static"));

var _config = _interopRequireDefault(require("./config"));

var _db = _interopRequireDefault(require("./entities/db"));

var _api = _interopRequireDefault(require("./routes/api"));

/**
 * Creates a Koa Application Server and connects to Postgres Server
 * @return {Promise<ServerInstance>} mapping of App, DB, and Server
 * @async
 * @function
 */
function createServer() {
  return _createServer.apply(this, arguments);
}

function _createServer() {
  _createServer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    var app, db, server, serverInstance;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            app = new _koa["default"]();
            _context.next = 3;
            return (0, _db["default"])();

          case 3:
            db = _context.sent;
            app.use((0, _cors["default"])({
              origin: '*'
            }));
            app.use((0, _koaHelmet["default"])());
            app.use((0, _koaBodyparser["default"])());
            app.use(_api["default"].routes());
            app.use(_api["default"].allowedMethods());
            app.use((0, _koaStatic["default"])('../client/dist'));
            server = app.listen(_config["default"].serverPort, function () {
              console.log("Server running on port ".concat(_config["default"].serverPort));
            });
            serverInstance = {
              app: app,
              db: db,
              server: server
            };
            return _context.abrupt("return", serverInstance);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createServer.apply(this, arguments);
}

var _default = createServer();

exports["default"] = _default;