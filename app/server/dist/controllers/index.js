"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.redirectToURL = redirectToURL;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _constants = require("../utils/constants.js");

function redirectToURL(_x) {
  return _redirectToURL.apply(this, arguments);
}

function _redirectToURL() {
  _redirectToURL = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(ctx) {
    var slug;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            slug = ctx.params.slug; // if the slug is less than 7 char we know we don't need to hit the db.
            // redirect back to the SPA

            if (slug.length < _constants.SLUG_LENGTH) {
              ctx.redirect('/');
            } // TODO: implement


            ctx.status = 200;
            ctx.body = {}; // TODO:
            //  - take slug and lookup if present
            //    - redirect if found, back to form if not

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _redirectToURL.apply(this, arguments);
}