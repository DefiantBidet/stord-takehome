"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkSlug = checkSlug;
exports.saveURL = saveURL;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _crypto = _interopRequireDefault(require("crypto"));

var _buffer = require("buffer");

var _uuid = require("uuid");

var _baseX = _interopRequireDefault(require("base-x"));

var _link = require("../entities/link");

var _constants = require("../utils/constants");

/**
 * Base62 Conversion via base-x
 * @param {[type]} BASE62_ALPHABET [A-Z][a-z][0-9]
 */
var baseConverter = (0, _baseX["default"])(_constants.BASE62_ALPHABET);
/**
 * Error Body for unfound slugs
 */

/**
 * Helper to return a common payload in unfound slug situations
 * @return {SlugNotFound} Error Payload
 * @function
 */
var returnNotFoundPayload = function returnNotFoundPayload() {
  var errorBody = {
    error: 'no slug found'
  }; // respond with 200/400?
  // 400 feels the REST thing to do
  // 200 doesn't notify the client things broke

  return {
    status: 200,
    body: errorBody
  };
};
/**
 * Generates a slug based on defined Max Character Length.
 * Slug generation is an MD5 hash of the URL, which is then converted
 * into a hexadecimal Buffer that is used to create a v4 UUID. The UUID
 * is then encoded via the Base62 encoder.
 * @param  {string} hash MD5 hash of URL
 * @return {string}      Base62 encoded slug
 * @function
 */


var generateSlug = function generateSlug(hash) {
  var buffer = _buffer.Buffer.from(hash, 'hex');

  var uuid = (0, _uuid.v4)(null, buffer);
  var decodedString = baseConverter.encode(uuid);
  return decodedString.slice(0, 7);
};
/**
 * Route Callback to save/write a Link.
 * Creates an MD5 hash of the URL, creates a Base62 slug, and
 * attempts to write to Database.
 * @param {ParameterizedContext} ctx  Route Context/Koa Context
 * @return {Promise<void>}
 * @async
 * @function
 */


function saveURL(_x) {
  return _saveURL.apply(this, arguments);
}
/**
 * Route Callback to read a Link.
 * Attempts to read from the Database.
 * @param {ParameterizedContext} ctx  Route Context/Koa Context
 * @return {Promise<void>}
 * @async
 * @function
 */


function _saveURL() {
  _saveURL = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(ctx) {
    var requestBody, url, hash, storedMD5, slug, link, results;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            requestBody = ctx.request.body;
            url = requestBody.url;
            hash = _crypto["default"].createHash('md5').update(url).digest('hex'); // check if md5 hash of link is stored in DB

            _context.next = 5;
            return (0, _link.getLinkByMD5)(hash);

          case 5:
            storedMD5 = _context.sent;

            if (!(storedMD5 === undefined)) {
              _context.next = 17;
              break;
            }

            slug = generateSlug(hash); // not already stored, save it.

            link = {
              md5: hash,
              slug: slug,
              url: url
            };
            _context.next = 11;
            return (0, _link.createLink)(link);

          case 11:
            results = _context.sent;

            if (!(results.rowCount < 1)) {
              _context.next = 14;
              break;
            }

            throw new Error('Error inserting Link');

          case 14:
            ctx.status = 200;
            ctx.body = link;
            return _context.abrupt("return");

          case 17:
            // return existing link
            ctx.status = 200;
            ctx.body = storedMD5;
            return _context.abrupt("return");

          case 20:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _saveURL.apply(this, arguments);
}

function checkSlug(_x2) {
  return _checkSlug.apply(this, arguments);
}

function _checkSlug() {
  _checkSlug = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(ctx) {
    var requestParams, slug, payload, storedSlug, _payload;

    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            requestParams = ctx.params;
            slug = requestParams.slug; // if the slug is less than 7 char we know we don't need to hit the db.

            if (!(slug.length < _constants.SLUG_LENGTH)) {
              _context2.next = 7;
              break;
            }

            payload = returnNotFoundPayload();
            ctx.status = payload.status;
            ctx.body = payload.body;
            return _context2.abrupt("return");

          case 7:
            _context2.next = 9;
            return (0, _link.getLinkBySlug)(slug);

          case 9:
            storedSlug = _context2.sent;

            if (!(storedSlug === undefined)) {
              _context2.next = 15;
              break;
            }

            _payload = returnNotFoundPayload();
            ctx.status = _payload.status;
            ctx.body = _payload.body;
            return _context2.abrupt("return");

          case 15:
            ctx.status = 200;
            ctx.body = storedSlug;
            return _context2.abrupt("return");

          case 18:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _checkSlug.apply(this, arguments);
}