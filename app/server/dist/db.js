"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _knex = _interopRequireDefault(require("knex"));

var _config = _interopRequireDefault(require("./config.js"));

var _default = (0, _knex["default"])({
  client: 'pg',
  connection: {
    host: '127.0.0.1',
    port: _config["default"].postgresPort,
    user: _config["default"].postgresUser,
    password: _config["default"].postgresPassword,
    database: _config["default"].postgresDb
  }
});

exports["default"] = _default;