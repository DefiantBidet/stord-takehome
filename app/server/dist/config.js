"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _dotenv = _interopRequireDefault(require("dotenv"));

_dotenv["default"].config({
  path: '../../.env'
});

var config = {
  environment: process.env.NODE_ENV || 'missing_env_variable',
  postgresDb: process.env.POSTGRES_DB || 'missing_env_variable',
  postgresHost: process.env.SERVER_HOST || 'missing_env_variable',
  postgresPassword: process.env.POSTGRES_PASSWORD || 'missing_env_variable',
  postgresPort: Number.parseInt(process.env.POSTGRES_PORT || '0', 10),
  postgresUser: process.env.POSTGRES_USER || 'missing_env_variable',
  serverHost: process.env.POSTGRES_HOST || 'missing_env_variable',
  serverPort: Number.parseInt(process.env.SERVER_PORT || '0', 10)
};
var _default = config;
exports["default"] = _default;