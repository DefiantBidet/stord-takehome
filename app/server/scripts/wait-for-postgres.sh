#!/bin/sh
# wait-for-postgres.sh

set -e
  
if [ ! ${POSTGRES_HOST} ]; then
  export $(grep -v '^#' ../../.env | xargs)
fi

command="nc -z ${POSTGRES_HOST} ${POSTGRES_PORT}"

while ! exec $(command); do
  sleep 1
done

exec "$@"
