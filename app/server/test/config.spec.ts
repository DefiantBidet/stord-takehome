import * as chai from 'chai';
import config, { Config } from '@server/config';

const assert = chai.assert;

describe('Config', () => {
  it('exports \'Config\' interface', () => {
    const mockConfig: Config = {
      environment: 'foo',
      port: 1234,
      postgresUser: 'foo',
      postgresPassword: 'bar',
    };

    assert.exists(mockConfig.environment, 'Config Interface expects \'environment\' member');
    assert.isString(mockConfig.environment, 'Expected \'environment\' member to be string');

    assert.exists(mockConfig.port, 'Config Interface expects \'port\' member');
    assert.isNumber(mockConfig.port, 'Expected \'port\' member to be number');

    assert.exists(mockConfig.postgresUser, 'Config Interface expects \'postgresUser\' member');
    assert.isString(mockConfig.postgresUser, 'Expected \'postgresUser\' member to be string');

    assert.exists(mockConfig.postgresPassword, 'Config Interface expects \'postgresPassword\' member');
    assert.isString(mockConfig.postgresPassword, 'Expected \'postgresPassword\' member to be string');
  });

  it('exports configuration object', () => {
    assert.exists(config.environment, 'Expected \'environment\' to be present in config');
    assert.exists(config.port, 'Expected \'port\' to be present in config');
    assert.exists(config.postgresUser, 'Expected \'postgresUser\' to be present in config');
    assert.exists(config.postgresPassword, 'Expected \'postgresPassword\' to be present in config');
  });
});
