import * as chai from 'chai';
import * as sinon from 'sinon';
import request from 'supertest';
import serverInstance from '@server/server';

const assert = chai.assert;

describe('Server', () => {
  let sandbox: sinon.SinonSandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  after(() => {
    serverInstance.server.close();
  });

  it('returns a \'ServerInstance\' object', async () => {
    assert.exists(serverInstance.app, 'Expected \'app\' to be present.');
    assert.exists(serverInstance.server, 'Expected \'server\' to be present.');
  });

  xit('loads docroot', async () => {
    const response = await request(serverInstance.app.callback()).get('/');
    assert.equal(response.status, 200);
    assert.equal(response.text, 'Hello');
  });
});
