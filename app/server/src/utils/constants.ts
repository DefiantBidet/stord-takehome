
/**
 * Base62 charset for slug generation
 * @type {String}
 */
export const BASE62_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

/**
 * Max length of slug
 * @type {Number}
 */
export const SLUG_LENGTH = 7;
