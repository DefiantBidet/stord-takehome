import dotenv from 'dotenv';

dotenv.config({ path: '../../.env' });

export interface Config {
  environment: string;
  postgresDb: string;
  postgresHost: string
  postgresPassword: string;
  postgresPort: number;
  postgresUser: string;
  serverHost: string
  serverPort: number;
}

const config: Config = {
  environment: process.env.NODE_ENV || 'missing_env_variable',
  postgresDb: process.env.POSTGRES_DB || 'missing_env_variable',
  postgresHost: process.env.SERVER_HOST || 'missing_env_variable',
  postgresPassword: process.env.POSTGRES_PASSWORD || 'missing_env_variable',
  postgresPort: Number.parseInt(process.env.POSTGRES_PORT || '0', 10),
  postgresUser: process.env.POSTGRES_USER || 'missing_env_variable',
  serverHost: process.env.POSTGRES_HOST || 'missing_env_variable',
  serverPort: Number.parseInt(process.env.SERVER_PORT || '0', 10),
};

export default config;
