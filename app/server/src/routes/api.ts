import Router from '@koa/router';

import { checkSlug, saveURL } from 'controllers/api';

const apiRouter = new Router({ prefix: '/api' });

/**
 * GET Attempts to retrieve information of stored links via slug.
 * @param {string}   '/:slug'  Path to handle with slug as route parameter
 * @param {function} checkSlug Controller method callback
 * @function
 */
apiRouter.get('/:slug', checkSlug);

/**
 * POST Attempts to generate a slug from supplied link.
 * @param {string}   '/generate' Path to handle
 * @param {function} saveURL     Controller method callback
 */
apiRouter.post('/generate', saveURL);

export default apiRouter;
