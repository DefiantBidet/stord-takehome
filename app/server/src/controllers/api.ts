import crypto from 'crypto';
import { Buffer } from 'buffer';

import { v4 } from 'uuid';
import { ParameterizedContext } from 'koa';
import { QueryResult } from 'pg';
import baseX from 'base-x';

import { createLink, getLinkByMD5, getLinkBySlug, Link } from 'entities/link';
import { BASE62_ALPHABET, SLUG_LENGTH } from 'utils/constants';

/**
 * Base62 Conversion via base-x
 * @param {[type]} BASE62_ALPHABET [A-Z][a-z][0-9]
 */
const baseConverter: baseX.BaseConverter = baseX(BASE62_ALPHABET);

/**
 * Error Body for unfound slugs
 */
interface ErrorBody {
  error: string;
}

/**
 * Not Found Data
 */
interface SlugNotFound {
  status: number;
  body: ErrorBody;
}

/**
 * Helper to return a common payload in unfound slug situations
 * @return {SlugNotFound} Error Payload
 * @function
 */
const returnNotFoundPayload = (): SlugNotFound => {
  const errorBody: ErrorBody = {
    error: 'no slug found',
  };

  // respond with 200/400?
  // 400 feels the REST thing to do
  // 200 doesn't notify the client things broke
  return {
    status: 200,
    body: errorBody,
  };
};

/**
 * Generates a slug based on defined Max Character Length.
 * Slug generation is an MD5 hash of the URL, which is then converted
 * into a hexadecimal Buffer that is used to create a v4 UUID. The UUID
 * is then encoded via the Base62 encoder.
 * @param  {string} hash MD5 hash of URL
 * @return {string}      Base62 encoded slug
 * @function
 */
const generateSlug = (hash: string): string => {
  const buffer = Buffer.from(hash, 'hex');
  const uuid = v4(null, buffer);
  const decodedString = baseConverter.encode(uuid);
  return decodedString.slice(0, 7);
};

/**
 * Route Callback to save/write a Link.
 * Creates an MD5 hash of the URL, creates a Base62 slug, and
 * attempts to write to Database.
 * @param {ParameterizedContext} ctx  Route Context/Koa Context
 * @return {Promise<void>}
 * @async
 * @function
 */
export async function saveURL(ctx: ParameterizedContext): Promise<void> {
  const requestBody: Record<string, string> = ctx.request.body as Record<string, string>;
  const url = requestBody.url;

  const hash = crypto.createHash('md5')
    .update(url)
    .digest('hex');

  // check if md5 hash of link is stored in DB
  const storedMD5 : Link | undefined = await getLinkByMD5(hash);

  if (storedMD5 === undefined) {
    const slug = generateSlug(hash);

    // not already stored, save it.
    const link: Link = {
      md5: hash,
      slug,
      url,
    };

    const results: QueryResult = await createLink(link);

    if (results.rowCount < 1) {
      throw new Error('Error inserting Link');
    }
    ctx.status = 200;
    ctx.body = link;
    return;

  }
  // return existing link
  ctx.status = 200;
  ctx.body = storedMD5;
  return;
}

/**
 * Route Callback to read a Link.
 * Attempts to read from the Database.
 * @param {ParameterizedContext} ctx  Route Context/Koa Context
 * @return {Promise<void>}
 * @async
 * @function
 */
export async function checkSlug(ctx: ParameterizedContext): Promise<void> {
  const requestParams: Record<string, string> = ctx.params as Record<string, string>;
  const slug = requestParams.slug;

  // if the slug is less than 7 char we know we don't need to hit the db.
  if (slug.length < SLUG_LENGTH) {
    const payload = returnNotFoundPayload();
    ctx.status = payload.status;
    ctx.body = payload.body;
    return;
  }

  // check if slug is stored in DB
  const storedSlug : Link | undefined = await getLinkBySlug(slug);

  if (storedSlug === undefined) {
    const payload = returnNotFoundPayload();
    ctx.status = payload.status;
    ctx.body = payload.body;
    return;
  }

  ctx.status = 200;
  ctx.body = storedSlug;
  return;
}
