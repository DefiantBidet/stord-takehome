import { Server } from 'http';

import { Knex } from 'knex';
import Koa from 'koa';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';
import helmet from 'koa-helmet';
import serve from 'koa-static';

import config from 'server/config';
import dbConnection from 'entities/db';
import apiRoutes from 'routes/api';

/**
 * ServerInstance Data
 */
export interface ServerInstance {
  app: Koa;
  db: Knex;
  server: Server;
}

/**
 * Creates a Koa Application Server and connects to Postgres Server
 * @return {Promise<ServerInstance>} mapping of App, DB, and Server
 * @async
 * @function
 */
async function createServer(): Promise<ServerInstance> {
  const app: Koa = new Koa();
  const db: Knex = await dbConnection();

  app.use(cors({origin: '*'}));
  app.use(helmet());
  app.use(bodyParser());

  app.use(apiRoutes.routes());
  app.use(apiRoutes.allowedMethods());

  app.use(serve('../client/dist'));

  const server = app.listen(config.serverPort, () => {
    console.log(`Server running on port ${config.serverPort}`);
  });

  const serverInstance: ServerInstance = {
    app,
    db,
    server,
  };

  return serverInstance;
}


export default createServer();
