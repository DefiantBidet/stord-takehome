import { DatabaseConnection } from 'entities/db';
import { QueryResult } from 'pg';

/**
 * Link Information
 */
export interface Link {
  id?: number;
  md5: string;
  slug: string;
  url: string;
  created_timestamp?: string;
}

/**
 * Asynchronous request to write to the database.
 * Attempts to store a new Link entry.
 * @param  {Link}                 link Link data to store
 * @return {Promise<QueryResult>}      Results of query
 * @async
 * @function
 */
export const createLink = async (link: Link): Promise<QueryResult> => {
  const result: QueryResult = await DatabaseConnection<Link>('link').insert(link);
  return result;
};

/**
 * Asynchronous request to read from the database.
 * Attempts to read an existing Link entry.
 * @param  {string}        value MD5 hash of URL to search
 * @return {Promise<Link>}       Results of query
 * @async
 * @function
 */
export const getLinkByMD5 = async (value: string): Promise<Link> => {
  const link: Link = await DatabaseConnection<Link>('link').where('md5', value).first();
  return link;
};

/**
 * Asynchronous request to read from the database.
 * Attempts to read an existing Link entry.
 * @param  {string}        value Slug of Link to search
 * @return {Promise<Link>}       Results of query
 * @async
 * @function
 */
export const getLinkBySlug = async (value: string): Promise<Link> => {
  const link: Link = await DatabaseConnection<Link>('link').where('slug', value).first();
  return link;
};
