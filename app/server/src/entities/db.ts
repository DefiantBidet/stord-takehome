import knex, { Knex } from 'knex';
import appConfig from 'server/config';
// import appConfig from '../config';

/**
 * Knex Database Connection
 * @type {Knex}
 */
export let DatabaseConnection: Knex = null;

const knexConfig: Knex.Config = {
  client: 'pg',
  connection: {
    host: appConfig.postgresHost,
    port: appConfig.postgresPort,
    user: appConfig.postgresUser,
    password: appConfig.postgresPassword,
    database: appConfig.postgresDb,
  },
};

/**
 * Asynchronously creates a connection to the Database via Knex
 * @return {Promise<Knex>} Knex connexection object
 * @async
 * @function
 */
export default async function create (): Promise<Knex> {
  const connection: Knex = knex(knexConfig);
  DatabaseConnection = connection;

  // attempt a simple query to verify connection
  try {
    await connection.raw('SELECT 1');
    return connection;
  } catch (error) {
    throw new Error('Error connecting Knex to Database. Ensure Database Connection details.');
  }
}
