CREATE DATABASE url_shortener
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8';

ALTER DATABASE url_shortener
    OWNER TO postgres;

\connect url_shortener
