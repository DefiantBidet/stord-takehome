\connect url_shortener

-- Tables
CREATE TABLE IF NOT EXISTS link(
  id SERIAL primary key NOT NULL,
  slug text NOT NULL UNIQUE,
  url text NOT NULL,
  md5 text NOT NULL UNIQUE,
  created_timestamp timestamp default now()
);
