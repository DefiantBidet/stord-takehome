import axios, { AxiosResponse } from 'axios';

import { RedirectResponseData, SlugResponseData } from 'types/Api';

const baseURL = 'http://localhost:3000';
const apiPath = '/api';

const commonHeaders = {
  'Content-type': 'application/json',
};

const fetch = axios.create({
  baseURL: `${baseURL}${apiPath}`,
  headers: commonHeaders,
});


/**
 * POST - Fetch backend resource to generate/write slug
 * @param  {string}  url URL to store
 * @return {Promise}     Axios Response Data
 * @async
 * @function
 */
export const generateSlug = async (url: string): Promise<AxiosResponse<SlugResponseData, any>> => {
  return fetch.post<SlugResponseData>('/generate', { url });
};

/**
 * GET - Requests information on supplied slug.
 * @param  {string}  slug Slug to verify
 * @return {Promise}      Axios Response Data
 * @async
 * @function
 */
export const checkSlug = async (slug: string): Promise<AxiosResponse<RedirectResponseData, any>> => {
  return fetch.get<RedirectResponseData>(`/${slug}`);
};
