import { useState } from 'react';
import CopyButton from 'components/Buttons/CopyButton';
import { SlugResponseData } from 'types/Api';

import styles from 'components/slugUI.scss';

/**
 * Helper method to generate shortened link based on environment
 * @param  {string} slug slug of shortLink
 * @return {string}      URL of shortLink
 */
const generateLink = (slug: string): string => {
  let shortLink = `http://`;

  if (NODE_ENV === 'development') {
    shortLink += `localhost`;
  } else {
    shortLink += `${SERVER_HOST}`;
  }

  if (SERVER_PORT !== '80') {
    shortLink += `:${SERVER_PORT}`;
  }

  shortLink += `/${slug}`;

  return shortLink;
};

/**
 * Props for SlugGeneratedView
 */
export interface SlugGeneratedViewProps {
  slugData: SlugResponseData;
};

/**
 * SlugGeneratedView displays the shortened link.
 * @param {SlugGeneratedViewProps} props Props of Component
 * @return {JSX.Element}
 * @function
 */
export default function SlugGeneratedView(props: SlugGeneratedViewProps): JSX.Element {
  const [link] = useState<string>(generateLink(props.slugData.slug));

  const copyToClipboard = async () => {
    try {
      await navigator.clipboard.writeText(link)
    } catch (error) {
      throw error;
    }
  };

  const clipboardSupported: boolean = navigator.clipboard ? true : false;

  return(
    <>
      <div className={styles.inputContainer}>
        <h1 className={styles.h1}>Shortened Link Created</h1>
        <div id='slug-link' className={styles.slug}>
          {link}
          {
            clipboardSupported &&
            <CopyButton
              onClick={copyToClipboard}
              label='Copy'
            />
          }
        </div>
        <div className={styles.startOver}>
          <a href='/'>Create Another</a>
        </div>
      </div>
    </>
  );
};
