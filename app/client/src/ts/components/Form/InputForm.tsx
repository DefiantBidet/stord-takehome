import { useState } from 'react';

import { generateSlug } from 'api';
import TextInput from 'components/TextInput';
import SendButton from 'components/Buttons/SendButton';
import { SlugResponseData } from 'types/Api';

import styles from 'components/inputForm.scss';

const invalidURLError = 'Please supply a valid URL (eg: http://domain.com or https://domain.com).';

/**
 * Props for InputForm
 */
export interface InputFormProps {
  onGenerateComplete: (data: SlugResponseData) => void;
};

/**
 * InputForm renders a Form to hold the inputs for generating a shortLink
 * @param {InputFormProps} props  Props of the Component
 * @return {JSX.Element}
 * @function
 */
export default function InputForm(props: InputFormProps): JSX.Element {
  const [validationError, setValidationError] = useState<string | null>(null);
  const [inputValue, setInputValue] = useState<string>('');
  const [isButtonActive, setIsButtonActive] = useState<boolean>(false);

  /**
   * Validators to run onBlur of the input
   * @param  {string}  value      input value
   * @param  {boolean} isRequired is the field a required field
   * @return {void}
   * @function
   */
  const onBlurValidation = (value: string, isRequired = false) => {
    if (isRequired && value.length === 0) {
      setValidationError(invalidURLError);
      setIsButtonActive(false);
      return;
    }

    // handle non URL strings
    try {
      new URL(value);
    } catch (error) {
      // not a valid URL
      setValidationError(invalidURLError);
      setIsButtonActive(false);
      return;
    }

    setIsButtonActive((validationError === null) && (inputValue.length !== 0));
  };

  /**
   * Input change handler
   * @param  {string} value Input value
   * @return {void}
   * @function
   */
  const onInputChange = (value: string): void => {
    setValidationError(null);
    setInputValue(value);
  }

  /**
   * Form submit handler
   * @return {Promise<void>}
   * @async
   * @function
   */
  const onSubmit = async (): Promise<void> => {
    if (!inputValue.trim().length) {
      // no value - don't submit
      return;
    }

    const response = await generateSlug(inputValue);
    props.onGenerateComplete(response.data);
  };

  return (
      <>
        <form
          className={styles.inputContainer}
          onSubmit={onSubmit}
          aria-labelledby='url-shortener'
        >
          <fieldset>
            <legend id='url-shortener' className={styles.legend}>Shorten URL</legend>
            <TextInput
              elementName='messageInput'
              error={validationError}
              placeHolder='Enter URL'
              onInput={onInputChange}
              required={true}
              validate={onBlurValidation}
            />
            <SendButton
              active={isButtonActive}
              label='Shorten'
              onClick={onSubmit}
            />
          </fieldset>
        </form>
      </>
    );
};
