import React from 'react';

import styles from 'components/copyButton.scss';

/**
 * Props for CopyButton
 */
export interface CopyButtonProps {
  label: string;
  onClick?: () => void;
};

/**
 * Copy Button renders a small button to copy
 * contents of a container to the clipboard.
 * @param {CopyButtonProps} props Props of Component
 * @return {JSX.Element}
 * @function
 */
export default function CopyButton(props: CopyButtonProps): JSX.Element {
  const onButtonClick = (event: React.SyntheticEvent): void => {
    event.stopPropagation();

    if (props.onClick) {
      props.onClick();
    }
  };

  return (
    <>
      <button
        type='button'
        className={styles.base}
        onClick={onButtonClick}
      >
        {props.label}
      </button>
    </>
  );
};
