import React, { useState } from 'react';

import styles from 'components/sendButton.scss';

/**
 * Props for SendButton
 */
export interface SendButtonProps {
  active: boolean;
  label: string;
  onClick?: () => void;
};

/**
 * SendButton renders a Form button to generate shortened URLs
 * @param {SendButtonProps} props Props of Component
 * @return {JSX.Element}
 * @function
 */
export default function SendButton(props: SendButtonProps): JSX.Element {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const className = props.active ? styles.activeButton : styles.inactiveButton;

  /**
   * Handles onClick event of button
   * @param  {React.SyntheticEvent} event Click Event
   * @return {void}
   * @function
   */
  const onButtonClick = (event: React.SyntheticEvent): void => {
    event.stopPropagation();

    if (!props.active) {
      return;
    }

    setIsLoading(true);
    if (props.onClick) {
      props.onClick();
    }
  };

  /**
   * Renders Loading Icon
   * @return {JSX.Element}
   * @function
   */
  const renderLoadingIcon = ():JSX.Element => {
    return (
      <>
      <div className={styles.loadingIndicator}>
        <svg className={styles.spin} role='img' aria-label='loading'>
            <use xlinkHref='#svg-loading-icon'></use>
          </svg>
        </div>
      </>
    );
  }

  return (
    <>
      <button
        type='button'
        className={className}
        onClick={onButtonClick}
      >
        {!isLoading && props.label}
        {isLoading && renderLoadingIcon()}
      </button>
    </>
  );
};
