import React, { useEffect, useState, useRef } from 'react';

import styles from 'components/textInput.scss';

/**
 * Helper utility to toggle error messages of field validation.
 * @param  {boolean} pristine has the field been touched
 * @param  {boolean} valid    is the field valid
 * @param  {boolean} required is the field required
 * @return {boolean}          Should the ErrorDisplay be visible
 * @function
 */
const showErrorDisplay= (pristine: boolean, valid: boolean, required: boolean): boolean => {
  if (!required || pristine || valid) {
    return false;
  }
  return true;
};

/**
 * Helper to toggle isValid
 * @param  {boolean}        isValid     is the field valid
 * @param  {string | null}  errorProp   component prop
 * @param  {function}       dispatch    hook callback/state dispatch
 * @return {void}
 * @function
 */
const toggleValidity = (isValid: boolean, errorProp: string | null, dispatch: (value: React.SetStateAction<boolean>) => void) => {
  if (isValid && errorProp !== null) {
    dispatch(false);
  }
};

/**
 * Determine styles for Input Valid/Error
 * @param  {string | null} error component prop value
 * @return {string}              class value
 */
const getInputClass = (error: string | null): string => {
  return error !== null ? styles.inputError : styles.inputValid;
};

/**
 * Props for TextInput
 */
export interface TextInputProps {
  elementName: string;
  error: string | null;
  inputValue?: string;
  onInput?: (value: string) => void;
  placeHolder: string;
  required?: boolean;
  validate?: (value: string, isRequired?:boolean) => void;
};

/**
 * TextInput renders a text input field
 * @param {TextInputProps} props Props of Component
 * @return {JSX.Element}
 * @function
 */
export default function TextInput(props: TextInputProps): JSX.Element {
  const [isPristine, setPristine] = useState<boolean>(true);
  const [isValid, setValid] = useState<boolean>(false);
  const [showPlaceHolder, setShowPlaceholder] = useState<boolean>(true);
  const [inputValue, setInputValue] = useState<string>(props.inputValue ?? '');
  const [inputClass, setInputClass] = useState<string>(styles.inputValid);

  const textInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (isPristine) {
      // field is untouched - don't show errors
      return;
    }

    setInputClass(getInputClass(props.error));

  }, [inputValue, isValid, isPristine]);

  toggleValidity(isValid, props.error, setValid);

  /**
   * onBlur event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Blur Event
   * @return {void}
   * @function
   */
  const onBlur = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    event.preventDefault();

    const trimmedValue = inputValue.trim();
    // handle trimmed string being different from supplied
    if (inputValue !== trimmedValue) {
      setInputValue(trimmedValue);
    }

    if(props.validate) {
      props.validate(trimmedValue, props.required);
      return;
    }

    toggleValidity(isValid, props.error, setValid);
    setShowPlaceholder(isPlaceHolderVisibile());
  };

  /**
   * onFocus event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Focus Event
   * @return {void}
   * @function
   */
  const onFocus = (event: React.ChangeEvent<HTMLInputElement>): void => {
    // calling stopPropagation in the capture phase prevents cursor display
    // @see https://bugzilla.mozilla.org/show_bug.cgi?id=509684
    // event.stopPropagation();
    event.preventDefault();

    // if pristine set to false
    if (isPristine) {
      setPristine(false);
    }

    // toggle validity to true onFocus
    // so it doesn't appear to user they're entering incorrect info
    setValid(true);
    setInputClass(styles.inputValid)
    setShowPlaceholder(isPlaceHolderVisibile());
  };

  /**
   * onInput event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Input Event
   * @return {void}
   * @function
   */
  const onInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    event.preventDefault();

    const { value } = event.target;

    setInputValue(value);
    setShowPlaceholder(isPlaceHolderVisibile())

    if (props.onInput) {
      props.onInput(value);
    }
  };

  /**
   * determine if input placeholder is present
   * @return {boolean}
   * @function
   */
  const isPlaceHolderVisibile = (): boolean => {
    if (!textInputRef.current) {
      return false;
    }

    const { value } = textInputRef.current;

    // if there is no value, placeholder should be visible
    return value.length === 0;
  };

  /**
   * Creates Error UI if errors are present
   * @return {JSX.Element | null}
   * @function
   */
  const createErrorDisplay = ():JSX.Element | null => {
    if(props.error === null) {
      return null;
    }

    return (
      <>
      <div className={styles.errorContainer}>
        <ul>
          <li>{props.error}</li>
        </ul>
      </div>
      </>
    );
  };

  /**
   * Creates Input Placeholder
   * @return {JSX.Element | null}
   * @function
   */
  const createPlaceholder = (): JSX.Element | null => {
    const { elementName, placeHolder } = props;
    const className = props.error !== null ? styles.placeHolderError : styles.placeHolder

    if (!showPlaceHolder) {
      return null;
    }

    return (
      <label
        aria-hidden="true"
        className={className}
        htmlFor={elementName}
      >
        {placeHolder}
      </label>
    );
  }

  /**
   * Creates Input Element
   * @return {JSX.Element}
   * @function
   */
  const createInput = (): JSX.Element => {
    const { elementName, placeHolder } = props;

    return (
      <input
        aria-required={true}
        aria-label={placeHolder}
        id={elementName}
        className={inputClass}
        name={elementName}
        onBlur={onBlur}
        onFocus={onFocus}
        onInput={onInput}
        type='text'
        ref={textInputRef}
        value={inputValue}
      />
    );
  }

  return (
    <>
      <div className={styles.inputWrapper}>
        <div className={styles.inputContainer}>
          {createInput()}
          {showPlaceHolder && createPlaceholder()}
        </div>
        {showErrorDisplay(isPristine, isValid, props.required ?? false) && createErrorDisplay()}
      </div>
    </>
  );
};
