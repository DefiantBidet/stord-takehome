/**
 * Payload to Create a slug
 */
export interface SlugRequest {
  url: string;
}

/**
 * Payload to Request a Link based on slug
 */
export interface RedirectRequest {
  slug: string;
}

/**
 * Axios Response Payload for Slugs
 */
export interface SlugResponse {
  data: SlugResponseData;
}

/**
 * Axios Response Payload for Redirects
 */
export interface RedirectResponse {
  data: RedirectResponseData;
}

/**
 * Link Response Data
 */
export interface SlugResponseData {
  id: number;
  md5: string;
  slug: string;
  url: string;
  created_timestamp: string;
}

/**
 * Redirect Error/Not Found Reponse
 */
export interface RedirectResponseError {
  error: string;
}

/**
 * Redirect Response Data
 * @type {SlugResponseData | RedirectResponseError}
 */
export type RedirectResponseData = SlugResponseData | RedirectResponseError;
