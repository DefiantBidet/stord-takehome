import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";

import { checkSlug } from 'api';
import { RedirectResponseData, SlugResponseData } from 'types/Api';

import styles from 'pages/redirectSlug.scss';

/**
 * Redirects client to supplied Link URL
 * @param  {SlugResponseData} link Link data from Database
 * @return {void}
 * @function
 */
const redirectToURL = (link: SlugResponseData): void => {
  // redirect to stored URL
  location.replace(link.url);
}

/**
 * Redirect Slug UI
 * Page Container displaying error information should the
 * redirect not be found.
 * @return {JSX.Element}
 * @function
 */
export default function RedirectSlug(): JSX.Element {
  const [isNotFound, setNotfound] = useState<boolean>(false);
  const params = useParams();

  useEffect(() => {
    const fetchSlug = async () => {
      const response = await checkSlug(params.slug ?? '');
      const responseData: RedirectResponseData = response.data;
      const isError = responseData.hasOwnProperty('error');

      // Error payload returned, show error UI
      if (isError) {
        setNotfound(true);
        return;
      }

      // returned data is a Link, not an Error
      redirectToURL(response.data as SlugResponseData)
    }

    fetchSlug()
      .catch(console.error);
  }, []);

  /**
   * Renders the Error UI if Link not dound
   * @return {JSX.Element}
   * @function
   */
  const renderErrorUI = (): JSX.Element => {
    return (
      <div className={styles.errorContainer}>
        <h1 className={styles.header}>Something went wrong...</h1>
        <p className={styles.copy}>We could not find the link you entered.</p>
        <div className={styles.startOver}>
          <a href='/'>Create A New Link</a>
        </div>
      </div>
    );
  };

  return (
    <>
      {isNotFound && renderErrorUI()}
    </>
  );
}
