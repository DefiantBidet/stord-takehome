import { useState } from 'react';

import InputForm from 'components/Form/InputForm';
import SlugGeneratedView from 'components/Form/SlugGenerated';
import { SlugResponseData } from 'types/Api';

import styles from 'pages/mainForm.scss';

/**
 * Main Form UI
 * Page Container displaying form to input url to shorten.
 * @return {JSX.Element}
 * @function
 */
export default function MainForm(): JSX.Element {
  const [showForm, setShowform] = useState<boolean>(true);
  const [response, setResponse] = useState<SlugResponseData | null>(null);

  const onShowGeneratedSlug = (response: SlugResponseData) => {
    setResponse(response);
    setShowform(false);
  };

  /**
   * Renders the Form UI
   * @return {JSX.Element}
   * @function
   */
  const renderFormUI = (): JSX.Element => <InputForm onGenerateComplete={onShowGeneratedSlug} />;


  /**
   * Renders the Generated Slug UI.
   * Displayed on success of form POST.
   * @return {JSX.Element}
   * @function
   */
  const renderGeneratedSlugUI = ():JSX.Element => {
    if (response === null) {
      return (
        <div>Something went wrong.</div>
      );
    }

    return (<SlugGeneratedView slugData={response}/>);
  };

  return (
    <>
      <div className={styles.base}>
        { showForm && renderFormUI()}
        { !showForm && renderGeneratedSlugUI()}
      </div>
    </>
  );
};
