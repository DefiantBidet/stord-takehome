import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import URLShortenForm from 'pages/MainForm';
import RedirectSlugPage from 'pages/RedirectSlug';

const appElement = document.querySelector('main#app');

ReactDOM.render(
  <>
    <BrowserRouter>
      <Routes>
        <Route index element={<URLShortenForm />} />
        <Route path='/:slug' element={<RedirectSlugPage />} />
      </Routes>
    </BrowserRouter>
  </>,
  appElement,
);



