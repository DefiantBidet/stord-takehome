// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'inputContainer': string;
  'legend': string;
  'spin': string;
}
export const cssExports: CssExports;
export default cssExports;
