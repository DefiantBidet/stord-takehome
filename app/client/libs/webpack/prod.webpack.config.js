const path = require('path');

const dotenv = require('dotenv');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

dotenv.config({ path: '../../.env' });

const webpackConfig = {
  mode: 'production',
  watch: false,
  entry: {
    app: [
      './src/ts/index.tsx'
    ],
    vendors: [
      'react',
      'react-dom',
    ],
  },
  output: {
    filename: 'stord-[name]-[chunkhash].js',
    path: path.resolve(process.cwd(), 'dist'),
    publicPath: '/',
    clean: true,
  },
  resolve: {
    roots: [path.resolve(process.cwd(), 'dist')],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.scss'],
    modules: ['src/ts', 'src/scss', 'node_modules'],
    alias: {
      "@api": path.resolve(path.resolve(process.cwd(), 'src/ts/api')),
      "@components": path.resolve(path.resolve(process.cwd(), 'src/ts/components')),
      "@pages": path.resolve(path.resolve(process.cwd(), 'src/ts/pages')),
      "@types": path.resolve(path.resolve(process.cwd(), 'src/ts/types')),
    },
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [
          'style-loader',
          'css-modules-typescript-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: true,
              importLoaders: 2,
            },
          },
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                outputStyle: 'expanded',
                includePaths: [
                  path.resolve(process.cwd(), 'src/scss'),
                  path.resolve(process.cwd(), 'node_modules/'),
                ],
              },
            },
          }
        ],
      },
      {
        test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
        use: ["file-loader"],
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ["ts-loader"],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'SERVER_PORT': JSON.stringify(process.env.SERVER_PORT),
      'SERVER_HOST': JSON.stringify(process.env.SERVER_HOST),
    }),
    new HtmlWebpackPlugin({
      template: './src/html/index.html'
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new MiniCssExtractPlugin({
      filename: 'stord-[name]-[chunkhash].css',
      chunkFilename: 'stord-[id]-[chunkhash].css',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
};

module.exports = webpackConfig;
