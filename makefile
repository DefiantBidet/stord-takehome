.PHONY: setup server test init-client init-db init-server test-client test-server

DB_PATH=./app/db
CLIENT_PATH=./app/client
SERVER_PATH=./app/server

# `make setup` will be used after cloning or downloading to fulfill
# dependencies, and setup the the project in an initial state.
# This is where you might download rubygems, node_modules, packages,
# compile code, build container images, initialize a database,
# anything else that needs to happen before your server is started
# for the first time
setup: init-db init-client init-server

# `make server` will be used after `make setup` in order to start
# an http server process that listens on any unreserved port
# of your choice (e.g. 8080).
server:
	docker-compose up

# `make test` will be used after `make setup` in order to run
# your test suite.
test:
	@echo 'not setup'

init-client:
	cd $(CLIENT_PATH); \
	npm ci; \
	npm run build

init-db:
	cd $(DB_PATH); \
	docker build -t url_shortener . ;

init-server:
	cd $(SERVER_PATH); \
	npm ci; \
	docker build -t url_shortener .

init-server-ci:
	cd $(SERVER_PATH); \

test-client:
	cd $(CLIENT_PATH); \
	npm test

test-server:
	cd $(SERVER_PATH); \
	npm test
